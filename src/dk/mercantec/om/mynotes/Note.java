package dk.mercantec.om.mynotes;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Note 
{
    public String title, content;
    public ArrayList<User> users, owners;
    public boolean isChanged = false;
    public int id;
    public Timestamp timeCreated, timeModified;
    
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public Note(int id, String title, String content, ArrayList<User> users, 
            ArrayList<User> owners, Timestamp timeCreated, Timestamp timeModified)
    {
        this.title = title;
        this.content = content;
        this.users = users;
        this.owners = owners;
        this.id = id;
        this.timeCreated = timeCreated;
        this.timeModified = timeModified;
    }
    
    
    //-------------------------------------------------
    // Function to format Timestamp, and return as a String
    //-------------------------------------------------
    public String timestampAsString(Timestamp date)
    {
        String s = " - ";
        if (date != null)
        {
            // do the formatting
            s = new SimpleDateFormat("dd/MM-yyyy HH:mm").format(date);
        }
        return "" + s;
    }
}
