package dk.mercantec.om.mynotes;

import java.io.File;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class XMLParser
{
    // variables used
    String username;
    String password;
    String file;
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public XMLParser()
    {
        // set file path to be in users home dir
        // this works in both Windows and Linux
        file = System.getProperty("user.home") + "//mynotes_conf.xml";
        
        // create new File object
        File f = new File(file);
        
        if(!f.exists()) 
        {
            // create file, with empty fields
            setConf("","");
        }
        else
        {
            // retrieve data from file
            getConf();
        }
    }
        
    //-------------------------------------------------
    // Function to create xml file, and update data
    //-------------------------------------------------
    public void setConf(String username, String password)
    {
        try
        {
            // Creates a builder element
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("mynotes");
            doc.appendChild(rootElement);

            // User elements
            Element staff = doc.createElement("User");
            rootElement.appendChild(staff);

            // Username elements
            Element firstname = doc.createElement("username");
            firstname.appendChild(doc.createTextNode(username));
            staff.appendChild(firstname);

            // Password elements
            Element lastname = doc.createElement("password");
            lastname.appendChild(doc.createTextNode(password));
            staff.appendChild(lastname);

            // write the content into xml format
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource data = new DOMSource(doc);
                    
            // write the file path
            StreamResult filepath = new StreamResult(new File(file));
                    
            // saves the file with data and filepath
            transformer.transform(data, filepath);
                    
            // updates the variables
            this.username = username;
            this.password = password;
        }
        catch (ParserConfigurationException e)
        {
            System.out.println(e.getMessage());
        }
        catch (TransformerException e)
        {
            System.out.println(e.getMessage());
        }
    }
        
    //-------------------------------------------------
    // Read from the xml file, and get username and password
    //-------------------------------------------------
    public void getConf()
    {
        try
        {
            File f = new File(file);
                
            // Creates a builder element
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(f);

            doc.getDocumentElement().normalize();
                
            // Find the first user element
            NodeList nList = doc.getElementsByTagName("User");
                
            // for each User
            for (int user = 0; user < nList.getLength(); user++)
            {
                Node nNode = nList.item(user);

                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) nNode;
                            
                    //Saves elements to variabels
                    username = eElement.getElementsByTagName("username").item(0).getTextContent();
                    password = eElement.getElementsByTagName("password").item(0).getTextContent();
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}