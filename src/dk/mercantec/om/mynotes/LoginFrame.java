package dk.mercantec.om.mynotes;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JButton;

public class LoginFrame extends JFrame {

    private JButton buttonLogin;
    private JButton buttonAddUser;
    private JCheckBox checkboxSave;
    private JLabel labelUser;
    private JLabel labelPass;
    private JLabel labelSave;
    private JPasswordField textFieldPass;
    private JTextField textFieldUser;
    
    // association with db handler
    private DBHandler db;
    
    private XMLParser xml;
    private boolean isConfSaved;
    
    // constructor
    public LoginFrame(DBHandler db) {
        
        super("MyNotes - login");
        
        // get the DBHandler
        this.db = db;
        
        // create new xml parser object
        xml = new XMLParser();
        
        // check to see if a config is saved
        if (xml.username.length() > 0)
        {
            isConfSaved = true;
        }
        else
        {
            isConfSaved = false;
        }
        
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        // create fields and labels
        textFieldUser = new JTextField();
        textFieldPass = new JPasswordField();
        labelUser = new JLabel();
        labelPass = new JLabel();
        checkboxSave = new JCheckBox();
        labelSave = new JLabel();

        // create buttons, with icon
        Icon loginIcon = new ImageIcon(getClass().getResource("unlock.png"));
        buttonLogin = new JButton("Login", loginIcon);
        buttonLogin.setMargin(new Insets(1,1,1,1));
        add(buttonLogin);
        Icon addUserIcon = new ImageIcon(getClass().getResource("add_user.png"));
        buttonAddUser = new JButton("Sign up", addUserIcon);
        buttonAddUser.setMargin(new Insets(1,1,1,1));
        add(buttonAddUser);
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelUser.setText("Username");
        labelPass.setText("Password");
        labelSave.setText("Save login");
        textFieldUser.setText(xml.username);
        textFieldPass.setText(xml.password);

        if (isConfSaved)
        {   // if a username is already saved, then keep
            // the checkbox as selected
            checkboxSave.setSelected(true);
        }

        // event handler for button
        ButtonHandler handler = new ButtonHandler();
        buttonLogin.addActionListener(handler);
        buttonAddUser.addActionListener(handler);
        textFieldUser.addActionListener(handler);
        textFieldPass.addActionListener(handler);
        
        // "GUI-Klumpen"
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelPass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelSave, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .addComponent(buttonAddUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(checkboxSave)
                    .addComponent(textFieldUser)
                    .addComponent(textFieldPass, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUser))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPass))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelSave)
                    .addComponent(checkboxSave))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonLogin)
                    .addComponent(buttonAddUser))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        // end of "GUI-Klumpen"

        pack();
        
        
        // don't allow risizing
        setResizable(false);
        
        // center the window at statup
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
        
        // set program logo
        ImageIcon image;
        image = new ImageIcon(getClass().getResource("mynotes_logo.png"));
        setIconImage(image.getImage());
    }

    
    
    //-------------------------------------------------
    // Event handler
    //-------------------------------------------------
    private class ButtonHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            
            if (e.getSource() == buttonAddUser)
            {
                // display "add user"-frame
                UserFrame uf = new UserFrame("Create new user", db);
                uf.setVisible(true);
            }
            else if (e.getSource() == buttonLogin)
            {
                // get values from textfields
                String username = textFieldUser.getText();
                String password = textFieldPass.getText();
                boolean saveConfig = checkboxSave.isSelected();
                
                boolean validated = false;

                if (saveConfig)
                {   // save username/password to xml file in user dir
                    xml.setConf(username, password);
                }
                else
                {   // clear saved data
                    xml.setConf("", "");
                }
                
                // check if username and password combination is correct
                validated = db.isPasswordCorrect(username, password);
            
                if (validated)
                {   // The username and password matches
                    db.login(username);
                
                    // close this login window
                    setVisible(false);
                
                    // open main window
                    MainFrame vd = new MainFrame(db);
                    vd.setVisible(true);
                }            
                else
                {   // The username and password does NOT match!
                    JOptionPane.showMessageDialog(null, String.format("Login failed!", e.getActionCommand()));
                }
            }
        }
    }
}
