package dk.mercantec.om.mynotes;

import java.util.Properties;
import java.sql.*;

public class MySqlJDBCConnection
{
    
    // The JDBC Connector Class.
    private static final String dbClassName = "com.mysql.jdbc.Driver";

    Connection connection;
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public MySqlJDBCConnection(String server, String database, String username, String password)
    {
        try
        {
            // Class.forName(xxx) loads the jdbc classes and
            // creates a drivermanager class factory
            Class.forName(dbClassName);

            // Properties for user and password.
            Properties p = new Properties();
            p.put("user", username);
            p.put("password", password);

            // Now try to connect
            connection = DriverManager.getConnection("jdbc:mysql://" + server + "/" + database, p);
        }
        catch (ClassNotFoundException|SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Function to check if a connection is active
    //-------------------------------------------------
    public boolean isConnected()
    {
        try
        {
            if (connection != null)
            {   // a Connection exists
                if (connection.getCatalog().replaceAll("\\s", "").equals(""))
                {   // not connected to a database
                    return false;
                }
                return !connection.isClosed();
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            return false;
        }
    }
}
