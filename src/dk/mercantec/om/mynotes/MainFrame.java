package dk.mercantec.om.mynotes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class MainFrame extends javax.swing.JFrame {

    // all the items
    private javax.swing.JButton BDelete;
    private javax.swing.JButton BExit;
    private javax.swing.JButton BSettings;
    private javax.swing.JButton BNew;
    private javax.swing.JButton BSave;
    private javax.swing.JButton BShare;
    private javax.swing.JList ListNote;
    private javax.swing.JList ListShare;
    private javax.swing.JTextArea TextNote;
    private javax.swing.JTextField TextTitle;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField textShare;
    private javax.swing.JButton bShareDelete;
    private javax.swing.JButton bLogout;
    private javax.swing.JButton bRefresh;
    private javax.swing.JLabel labelSave;
    private javax.swing.JLabel labelDate;
    private javax.swing.JLabel labelShare;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JLabel labelNotification;
    
    // ArrayList with the users notes
    ArrayList<Note> notes;
    
    // The DB handler
    DBHandler db;
    
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public MainFrame(DBHandler db) {
        
        // call superclass' constructor to set title
        super("MyNotes - Logged in as " + db.loggedInUser.getFullName());
        
        this.db = db;
        
        // call the main function that sets up the frame
        initComponents();
        
        // don't allow risizing
        setResizable(false);
        
        // center the window at statup
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
    }

    
    //-------------------------------------------------
    // Main function that displays all the content
    //-------------------------------------------------
    @SuppressWarnings("unchecked")
    private void initComponents() {

        // create all the elements
        jScrollPane1 = new javax.swing.JScrollPane();
        ListNote = new javax.swing.JList();
        TextTitle = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        TextNote = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListShare = new javax.swing.JList();
        textShare = new javax.swing.JTextField();
        labelSave = new javax.swing.JLabel();
        labelDate = new javax.swing.JLabel();
        labelShare = new javax.swing.JLabel();
        labelTitle = new javax.swing.JLabel();
        labelNotification = new javax.swing.JLabel();
        
        // buttons, with icons
        Icon saveIcon = new ImageIcon(getClass().getResource("save.png"));
        BSave = new JButton("Save", saveIcon);
        Icon deleteIcon = new ImageIcon(getClass().getResource("delete.png"));
        BDelete = new JButton("Delete", deleteIcon);
        Icon newNoteIcon = new ImageIcon(getClass().getResource("add.png"));
        BNew = new JButton("New note", newNoteIcon);
        Icon exitIcon = new ImageIcon(getClass().getResource("quit.png"));
        BExit = new JButton("Exit", exitIcon);
        Icon settingsIcon = new ImageIcon(getClass().getResource("settings.png"));
        BSettings = new JButton("Options", settingsIcon);
        Icon shareIcon = new ImageIcon(getClass().getResource("add_user.png"));
        BShare = new JButton("", shareIcon);
        Icon deleteShareIcon = new ImageIcon(getClass().getResource("delete.png"));
        bShareDelete = new JButton("", deleteShareIcon);
        Icon logoutIcon = new ImageIcon(getClass().getResource("login.png"));
        bLogout = new JButton("Logout", logoutIcon);
        Icon refreshIcon = new ImageIcon(getClass().getResource("refresh.png"));
        bRefresh = new JButton("Refresh", refreshIcon);

        // set padding on buttons
        BSave.setMargin(new Insets(1,1,1,1));
        BDelete.setMargin(new Insets(1,1,1,1));
        BNew.setMargin(new Insets(1,1,1,1));
        BExit.setMargin(new Insets(1,1,1,1));
        BShare.setMargin(new Insets(1,1,1,1));
        bShareDelete.setMargin(new Insets(1,1,1,1));
        bLogout.setMargin(new Insets(1,1,1,1));
        bRefresh.setMargin(new Insets(1,1,1,1));
        BSettings.setMargin(new Insets(1,1,1,1));

        // set the program to exit on click on X
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        ListNote.setFixedCellWidth(20);
        jScrollPane1.setViewportView(ListNote);

        // set text on labels
        labelSave.setText("Date Created:");
        labelDate.setText("Last Save:");
        labelShare.setText("Share:");
        labelTitle.setText("Title:");
        
        // Note content text field options
        TextNote.setColumns(20);
        TextNote.setRows(5);
        TextNote.setLineWrap(true);
        
        // scroll panes
        jScrollPane2.setViewportView(TextNote);
        jScrollPane3.setViewportView(ListShare);

        // "GUI-Klumpen"
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BNew, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(BSettings, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bLogout, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BExit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelTitle)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TextTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(BDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(8, 8, 8)
                                        .addComponent(labelDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(bRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(8, 8, 8)
                                        .addComponent(labelSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelShare, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(textShare, javax.swing.GroupLayout.Alignment.TRAILING, 80, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(BShare, javax.swing.GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bShareDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2)
                .addComponent(labelNotification))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BExit)
                            .addComponent(BSettings)
                            .addComponent(bLogout)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(TextTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(BNew)
                                    .addComponent(labelTitle)
                                    .addComponent(labelShare))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(textShare, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(BDelete)
                                                .addComponent(labelDate)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(BShare)
                                                .addComponent(bShareDelete))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(bRefresh)
                                                .addComponent(labelSave))))
                                    .addComponent(BSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)
                .addComponent(labelNotification)))
                .addContainerGap())
        );
        // end of "GUI-Klumpen"

        // add event handlers
        ButtonHandler buttonHandler = new ButtonHandler();
        ListHandler listHandler = new ListHandler();
        ListNote.addListSelectionListener(listHandler);
        ListShare.addListSelectionListener(listHandler);
        BExit.addActionListener(buttonHandler);
        BSettings.addActionListener(buttonHandler);
        BNew.addActionListener(buttonHandler);
        BSave.addActionListener(buttonHandler);
        BDelete.addActionListener(buttonHandler);
        BShare.addActionListener(buttonHandler);
        bLogout.addActionListener(buttonHandler);
        bRefresh.addActionListener(buttonHandler);
        bShareDelete.addActionListener(buttonHandler);
        
        pack();
        
        // set program logo
        ImageIcon image;
        image = new ImageIcon(getClass().getResource("mynotes_logo.png"));
        setIconImage(image.getImage());
        
        // update Note list view with data
        updateNoteListView(0);
    }

    
    //-------------------------------------------------
    // Event handler for all the buttons
    //-------------------------------------------------
    private class ButtonHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            //----- "New"-button -----
            if (e.getSource() == BNew)
            {
                // only allow 20 notes per user to avoid spam
                if (notes.size() >= 20)
                {   // update notification
                    updateNotification("Note could not be created! (To avoid spam, "
                            + "it is only allowed to have 20 notes.)", true);
                }
                else
                {
                    //  Add new note to database
                    int newNoteID = db.createNewNote();
                
                    // refresh notes list
                    notes = db.getAllNotesForUser(db.loggedInUser);

                    // update the list view
                    updateNoteListView(getListIndexForNote(newNoteID));
                    
                    // update notification
                    updateNotification("New note created", true);
                }
            }
            //----- "Exit"-button -----
            else if (e.getSource() == BExit)
            {
                // Ask for confirmation of possibly lost data
                int answer = JOptionPane.showConfirmDialog( null, 
                        "Any unsaved changes will be lost! Exit?",
                        "alert", JOptionPane.OK_CANCEL_OPTION);
                if (answer == 0) // OK
                {
                    // exit the program
                    System.exit(0);
                }
            }
            //----- "Save"-button -----
            else if (e.getSource() == BSave)
            {
                // get values
                String newTitle = TextTitle.getText();
                String newContent = TextNote.getText();
                int listPos = ListNote.getSelectedIndex();
                Note currentNote = notes.get(listPos);
                ArrayList users = currentNote.users;
                ArrayList owners = currentNote.owners;
                
                // update database with new values
                db.updateNote(currentNote, newTitle, newContent, users, owners);
                
                // update note list view
                updateNoteListView(listPos);
                
                // update notification
                updateNotification("Note successfully saved", true);
            }
            //----- "Delete"-button -----
            else if (e.getSource() == BDelete)
            {
                // get current index in list
                int index = ListNote.getSelectedIndex();
                
                int ok = 0;
                
                // if the note is shared with anyone, then ask for confirmation
                // before the note is deleted
                if (notes.get(index).users.size() > 0)
                {
                    // Answer yes = 0
                    // Answer no = 1
                    ok = JOptionPane.showConfirmDialog( null, 
                            "This note is shared with other users!\n\n"
                            + "Are you sure you want to delete it?",
                        "alert", JOptionPane.YES_NO_OPTION);
                }

                if (ok == 0)
                {
                    // delete the Note
                    db.deleteNote(getNoteIdInList(index));
                
                    // make sure we are not out of bounds
                    if (index < 1) index = 1;
                
                    // update list view, and go to index before the deleted one
                    updateNoteListView(index-1);
                    
                    // update notification
                    updateNotification("Note successfully removed", true);
                }
            }
            //----- "Share"-button -----
            else if (e.getSource() == BShare)
            {
                // get content of text field
                String shareUsername = textShare.getText();

                // If the user entered does not exist...
                if (!db.userExists(shareUsername))
                {   //... then update notification
                    updateNotification("There is no user named '" + shareUsername + "'!", false);
                }
                else
                {
                    // get User from username
                    User shareUser = db.getUserByUsername(shareUsername);
                    
                    // get current index in list
                    int index = ListNote.getSelectedIndex();
                    
                    // if proposed user is already in the list...
                    if (userIsInUsers(shareUser, notes.get(index).users))
                    {   //... then update notification
                        updateNotification("The note is already shared with '" + shareUsername + "'!", false);
                    }
                    // else if proposed user is yourself...
                    else if (db.loggedInUser.id == shareUser.id)
                    {   //... then update notification
                        updateNotification("You cannot share it with yourself!", false);
                    }
                    else
                    {
                        // save title and content in variables, to
                        // be able to restore after refreshing the list
                        String title = TextTitle.getText();
                        String note = TextNote.getText();
                        
                        // share the note
                        db.shareWithUser(getSelectedNote(), shareUser, true);
                    
                        // clear text field
                        textShare.setText("");
                        
                        // refresh notelist
                        updateNoteListView(ListNote.getSelectedIndex());
                        
                        // restore saved content fom before list was updated
                        TextTitle.setText(title);
                        TextNote.setText(note);
                        
                        // update notification
                        updateNotification("user " + shareUser.username + " succesfully added", true);
                    }
                }
            }
            //----- "Share delete"-button -----
            else if (e.getSource() == bShareDelete)
            {
                // get position in list view
                int index = ListShare.getSelectedIndex();
                
                if (index >= 0)
                {
                    // get the User object
                    User user = notes.get(ListNote.getSelectedIndex()).users.get(index);
                
                    // remove the user from the note
                    db.removeSharedUser(user, getSelectedNote());
                
                    // refresh note list
                    updateNoteListView(ListNote.getSelectedIndex());
                    
                    // update notification
                    updateNotification("user " + user.username + " succesfully removed", true);
                }
                else
                {
                    // update notification
                    updateNotification("no user selected", false);
                }
            }
            //----- "logout"-button -----
            else if (e.getSource() == bLogout)
            {
                // Ask for confirmation of possibly lost data
                int answer = JOptionPane.showConfirmDialog( null, 
                        "Any unsaved changes will be lost! Logout?",
                        "alert", JOptionPane.OK_CANCEL_OPTION);
                if (answer == 0) // 0 = "OK"
                {
                    // log out
                    db.logout();
                    
                    // close this window
                    setVisible(false);
                
                    // open login window
                    LoginFrame vd = new LoginFrame(db);
                    vd.setVisible(true);
                }
            }
            //----- "refresh"-button -----
            else if (e.getSource() == bRefresh)
            {
                // reload list of notes
                updateNoteListView(ListNote.getSelectedIndex());
                
                // update notification
                updateNotification("notes succesfully reloaded", true);
            }
            //----- "Settings"-button -----
            else if (e.getSource() == BSettings)
            {
                // display "edit user"-frame
                UserFrame uf = new UserFrame("Settings", db);
                uf.setVisible(true);
            }
                
        }
    }
    
    
    //-------------------------------------------------
    // event handler for the list views
    //-------------------------------------------------
    private class ListHandler implements ListSelectionListener
    {
        public void valueChanged(ListSelectionEvent e)
        {
            //----- Notes-list -----
            if (e.getSource() == ListNote)
            {
                if (ListNote.getSelectedIndex() >= 0)
                {
                    // call function that takes care of filling fields in right 
                    // side of the window
                    noteSelected(ListNote.getSelectedIndex());
                }
            }
            //----- Share-list -----
            else if (e.getSource() == ListShare)
            {
                // nothing needed to do here...
            }
        }
    }
    
    
    //-------------------------------------------------
    // Function to update note view (right side), when
    // a note is selected
    //-------------------------------------------------
    private void noteSelected(int index)
    {
        // get selected Note object
        Note note = notes.get(index);
        
        // set title
        TextTitle.setText(note.title);
        
        // set content
        TextNote.setText(note.content);

        // clear "add share"-field
        textShare.setText("");
        
        // set dates in labels
        labelDate.setText("Created: " + note.timestampAsString(note.timeCreated));
        labelSave.setText("Last save: " + note.timestampAsString(note.timeModified));
        
        // set users the note is shared with
        updateShareListView(note.users);
        
        // update notification (clear)
        updateNotification("", true);
    }
    
    
    //-------------------------------------------------
    // Function to update the Note List View
    //-------------------------------------------------
    private void updateNoteListView(int position)
    {
        if (position < 0) position = 0;

        // get all the notes that belongs to the logged in user
        notes = db.getAllNotesForUser(db.loggedInUser);
        
        // create default list model
        DefaultListModel<String> model = new DefaultListModel<String>();
        
        for(Note n : notes)
        {
            // add note title to model
            model.addElement(n.title);
        }
        
        // display elements in note list view
        ListNote.setModel(model);
        
        // set which element to be highlighted/selected
        ListNote.setSelectedIndex(position);
    }
    
    
    //-------------------------------------------------
    // Function to update the Share List View
    //-------------------------------------------------
    private void updateShareListView(ArrayList<User> users)
    {
        // create new default list model
        DefaultListModel<String> model = new DefaultListModel<String>();
     
        for (User u : users)
        {
            // add the usernames to the model
            model.addElement(u.username);
        }

        // update content of the share list view with the model
        ListShare.setModel(model);
    }
    
    
    //-------------------------------------------------
    // Function to get Note ID from position in ArrayList
    //-------------------------------------------------
    private int getNoteIdInList(int position)
    {
        Note note = notes.get(position);
        return note.id;
    }
    
    
    //-------------------------------------------------
    // Function to find position in array for a note
    //-------------------------------------------------
    private int getListIndexForNote(int noteID)
    {
        int i = 0;
        for (Note n : notes)
        {
            if (n.id == noteID)
            {
                // return index
                return i;
            }
            i++;
        }
        
        // if not found, then return 0
        return 0;
    }

    
    //-------------------------------------------------
    // Function to get highlighted note
    //-------------------------------------------------
    private Note getSelectedNote()
    {
        // get current index in list
        int index = ListNote.getSelectedIndex();
        
        // return the note
        return notes.get(index);
    }
    
    
    //-------------------------------------------------
    // Check if a user is in users list
    //-------------------------------------------------
    private boolean userIsInUsers(User user, ArrayList<User> users)
    {
        // loop through all users
        for (User u : users)
        {
            if (u.id == user.id)
            {   // if the specific user is found
                // then return true...
                return true;
            }
        }
        // else, return false
        return false;
    }
    
    
    private void updateNotification(String message, boolean successful)
    {
        if (successful)
        {   // Green text color
            labelNotification.setForeground(new Color(0, 231, 0));
        }
        else
        {   // Orange text color
            labelNotification.setForeground(new Color(255, 163, 51));
        }
        
        // update notification
        labelNotification.setText(message);
    }
}
