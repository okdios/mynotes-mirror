package dk.mercantec.om.mynotes;

import java.sql.*;
import java.util.ArrayList;

public class DBHandler extends MySqlJDBCConnection {
    
    ArrayList<User> users = new ArrayList();
    boolean loggedIn = false;
    User loggedInUser;
    
    
    //---------------------------------------------------------
    // Constructor
    //---------------------------------------------------------
    public DBHandler(String host, String database, String username, String password) throws
                             ClassNotFoundException,SQLException
    {
        // connect to the database
        super(host, database, username, password);
    }
    
    
    //---------------------------------------------------------
    // Log in for a user
    //---------------------------------------------------------
    public void login(String username)
    {
        // get User from username
        this.loggedInUser = this.getUserByUsername(username);
        
        this.loggedIn = true;
    }
    
    
    //---------------------------------------------------------
    // Log out for a user
    //---------------------------------------------------------
    public void logout()
    {
        // get User from username
        this.loggedInUser = null;
        
        this.loggedIn = false;
    }
    
    
    //---------------------------------------------------------
    // Check if a user is logged in
    //---------------------------------------------------------
    public boolean isLoggedIn()
    {
        return this.loggedIn;
    }
    
    
    //---------------------------------------------------------
    // Get an ArrayList of users that match the query
    //---------------------------------------------------------
    public ArrayList<User> getUsers(String query)
    {
        ArrayList<User> users = new ArrayList<User>();
        
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // get resultset
            ResultSet results = stmt.executeQuery(""
                    + "SELECT firstname, lastname, email, "
                    + "username, id "
                    + "FROM user " + query + ";");
        
            while (results.next()) // loop through resultset
            {
                // create new user object...
                User user = new User(results.getString(1),
                                     results.getString(2), 
                                     results.getString(3),  
                                     results.getString(4),
                                     results.getInt(5));
                // .. and add to ArrayList users
                users.add(user);
            }
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        // return all users
        return users;
    }
    
    
    //-------------------------------------------------
    // Get a User object from a username
    //-------------------------------------------------
    public User getUserByUsername(String username)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // fetch data from database
            ResultSet results = stmt.executeQuery(""
                    + "SELECT firstname, lastname, email, username, id "
                    + "FROM user "
                    + "WHERE  BINARY username = '" + username + "';");
        
            if (results.next())
            {
                // return the User object
                return new User(results.getString(1), results.getString(2),
                    results.getString(3), results.getString(4), results.getInt(5));
            }
            
            // close the connection
            stmt.close();
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
        
        return new User("", "", "", "", 0);
    }
    
    
    //-------------------------------------------------
    // Get a ArrayList of all users
    //-------------------------------------------------
    public ArrayList<User> getAllUsers()
    {
        return this.getUsers("");
    }
    
    
    //-------------------------------------------------
    // Create a new user
    //-------------------------------------------------
    public void createUser(String firstname, String lastname, String email, String username, String password)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // insert user into database
            stmt.execute("INSERT INTO user (firstname, lastname, email, username, password) "
                    + "VALUES ("
                    + "'" + firstname + "', "
                    + "'" + lastname + "', "
                    + "'" + email + "', "
                    + "'" + username + "', SHA1('" + password + "'));");
            
            // close the connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    //-------------------------------------------------
    // Change the password for a user
    //-------------------------------------------------
    public void changePassword(User user, String newPassword)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // fetch data
            stmt.executeUpdate("UPDATE user SET password=SHA1('" + newPassword + "') "
                    + "WHERE id = " + user.id + ";");
        
            // close the connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Update an existing user
    //-------------------------------------------------
    public void updateUser(User user, String newFirstname, String newLastname, String newEmail)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // update table
            stmt.executeUpdate("UPDATE user SET "
                    + "firstname = '" + newFirstname + "', "
                    + "lastname = '" + newLastname + "', "
                    + "email = '" + newEmail + "' "
                    + "WHERE id = " + user.id + ";");
            
            // close connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Check if a user exists in the db
    //-------------------------------------------------
    public boolean userExists(String username)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // fetch data
            ResultSet r = stmt.executeQuery(""
                    + "SELECT id "
                    + "FROM user "
                    + "WHERE BINARY username = '" + username + "';");
        
            if (r.next())
            {   // if there is a value, then return true
                stmt.close();
                return true;
            }
            
            // close connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        
        return false;
    }
    
    
    //-------------------------------------------------
    // Check if the username/password is correct
    //-------------------------------------------------
    public boolean isPasswordCorrect(String username, String password)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            ResultSet r = stmt.executeQuery(""
                    + "SELECT id "
                    + "FROM user "
                    + "WHERE BINARY username = '" + username + "' "
                    + "AND password = SHA1('" + password + "');");

            if (r.next())
            {   // if there is a value, then return true
                stmt.close();
                return true;
            }
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        
        return false;
    }
    
    
    //-------------------------------------------------
    // Get all notes that belongs to a certain user
    //-------------------------------------------------
    public ArrayList<Note> getAllNotesForUser(User user)
    {
        ArrayList<Note> notes = new ArrayList<Note>();
        ArrayList<User> users, owners;
        int id;
        java.sql.Timestamp timeCreated, timeModified;
        String title, content;
        
        try
        {
            // create statements
            Statement stmt = connection.createStatement();
            Statement stmt2 = connection.createStatement();
            
            // get all notes for this user
            ResultSet rNotes = stmt.executeQuery(""
                    + "CALL getNotesForUserID(" + user.id + ");");
        
            while (rNotes.next())
            {
                id = rNotes.getInt(1);
                title = rNotes.getString(2);
                content = rNotes.getString(3);
                timeCreated = rNotes.getTimestamp(4);
                timeModified = rNotes.getTimestamp(5);
            
                // get all users for this note (except for the logged in user)
                ResultSet rUsers = stmt2.executeQuery(""
                        + "CALL getUsersForNoteID(" + rNotes.getInt(1) + ", " 
                                                    + user.id + ");");

                users = new ArrayList();
                owners = new ArrayList();
                while (rUsers.next())
                {
                    // add users
                    users.add(new User(rUsers.getString(1), rUsers.getString(2), 
                    rUsers.getString(3), rUsers.getString(4), rUsers.getInt(5)));
                
                    // add owners
                    if (rUsers.getInt(6) == 1)
                    {
                        owners.add(new User(rUsers.getString(1), rUsers.getString(2), 
                        rUsers.getString(3), rUsers.getString(4), rUsers.getInt(5)));
                    }
                }

                // add note
                notes.add(new Note(id, title, content, users, owners, timeCreated, timeModified));
            }
            // close connections
            stmt.close();
            stmt2.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        return notes;
    }
    
    
    //-------------------------------------------------
    // Create a note (add to database)
    // returns id of new note
    //-------------------------------------------------
    public int addNoteToUser(String newTitle, String newContent, User user)
    {
        int insertNoteID = -1;
        // TODO
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            Statement stmt2 = connection.createStatement();
            
            //ResultSet result = stmt.executeQuery("CALL addNoteToUser('" + newTitle + "', '" + newContent + "', " + user.id + ");");
            stmt.execute("INSERT INTO note (title, content, timeCreated) "
                    + "VALUES ('" + newTitle + "', '" + newContent + "', "
                    + "CURRENT_TIMESTAMP);", Statement.RETURN_GENERATED_KEYS);

            ResultSet keyset = stmt.getGeneratedKeys();
            
            if (keyset.next())
            {
                // get last inserted id
                insertNoteID = keyset.getInt(1);
                
                // insert user / note relation
                stmt2.execute("INSERT INTO noteUser (noteID, userID, isOwner) "
                        + "VALUES (" + insertNoteID + ", " + user.id + ", 1);");
            }
            
            // close connections
            stmt.close();
            stmt2.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        
        // return the id of the newly created note
        return insertNoteID;
    }
    
    
    //-------------------------------------------------
    // Create a new (almost) blank note for current user 
    //-------------------------------------------------
    public int createNewNote()
    {
        return addNoteToUser("New note", "", this.loggedInUser);
    }
    
    
    //-------------------------------------------------
    // Update a note (update in database)
    //-------------------------------------------------
    public void updateNote(Note note, String newTitle, String newContent, 
            ArrayList<User> newUsers, ArrayList<User> newOwners)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            Statement stmt2 = connection.createStatement();

            // update note
            stmt.execute("UPDATE note SET "
                    + "title = '" + newTitle + "', "
                    + "content = '" + newContent + "' "
                    + "WHERE id = " + note.id + ";");

            // close statements
            stmt.close();
            stmt2.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Reset a note (reload data from database)
    //-------------------------------------------------
    public Note discardChangesToNote(Note oldNote)
    {
        // TODO
        /*
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // get all notes for this user
            ResultSet rNotes = stmt.executeQuery("SELECT note.id, note.title, note.content FROM note;");
        
            while (rNotes.next())
            {
                // TODO
                // get users/owners...
            }
            
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        */
        // get content from database
        return oldNote;
    }
    
    
    //-------------------------------------------------
    // Delete a note (remove from database, including users)
    // Input: note ID
    //-------------------------------------------------
    public void deleteNote(int noteID)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
            
            // execite stored procedure that removes the user
            stmt.execute("CALL removeNote(" + noteID + ");");
         
            // close connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Share a note with a user
    //-------------------------------------------------
    public void shareWithUser(Note note, User user, boolean userIsOwner)
    {
        // option to select whether the user should be a owner or not.
        // The "owner" role is not implemented yet.
        int isOwner = 0;
        if (userIsOwner)
        {
            isOwner = 1;
        }

        try
        {
            // create statement
            Statement stmt = connection.createStatement();
        
            // insert the note/user relation
            stmt.execute("INSERT INTO noteUser (noteID, userID, isOwner) "
                    + "VALUES (" + note.id+ ", " + user.id + ", " + isOwner + ");");
            
            // close the connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    //-------------------------------------------------
    // Remove shared user
    //-------------------------------------------------
    public void removeSharedUser(User user, Note note)
    {
        try
        {
            // create statement
            Statement stmt = connection.createStatement();
        
            // delete the user/note relation
            stmt.execute("DELETE FROM noteUser WHERE noteID = " + note.id + " "
                    + "AND userID = " + user.id + ";");
            
            // close the connection
            stmt.close();
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    
    
    
    
    
    //***********************************************************************
    //********** Functions for debugging purposes only **********************
    //***********************************************************************
    
    //-------------------------------------------------
    // TEST: Print all users currently in the database
    //-------------------------------------------------
    public void printAllUsers()
    {
        ArrayList<User> users = this.getAllUsers();
        
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("All users:");
        for (User u : users)
        {
            System.out.println(" - " + u.firstname + " " + u.lastname + " (" + u.username + ")");
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
    
    
    //-------------------------------------------------
    // TEST: print all notes
    //-------------------------------------------------
    public void printAllNotesForUser(User user)
    {
        ArrayList<Note> notes = this.getAllNotesForUser(user);
        
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("All notes for user '" + user.username + "':");
        for (Note n : notes)
        {
            System.out.print(" - " + n.title + " (" + n.timeCreated + " - " + n.timeModified + ")");
            
            // print users, if more than one
            if (n.users.size() > 1)
            {
                System.out.print(" (has multiple users: [" + this.implodeUsers(n.users, ", ") + "]");
                System.out.println(" - owner(s): [" + this.implodeUsers(n.owners, ", ") + "])");
            }
            else
            {
                System.out.println();
            }
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
    
    
    //-------------------------------------------------
    // TEST: create string from ArrayList<User>
    //-------------------------------------------------
    public String implodeUsers(ArrayList<User> users, String delimeter)
    {
        int i = 0;
        String string = "";
        
        for (User s : users)
        {
            if (i > 0)
            {
                string += delimeter;
            }
            
            string += s.username;

            i++;
        }
        
        return string;
    }
}
