package dk.mercantec.om.mynotes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

public class UserFrame extends javax.swing.JFrame {

    // Variables declaration
    private javax.swing.JButton jButtonSave;
    private javax.swing.JLabel jLabelFirstname;
    private javax.swing.JLabel jLabelLastname;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelUsername;
    private javax.swing.JLabel jLabelPassword1;
    private javax.swing.JLabel jLabelPassword2;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JPasswordField jPasswordField2;
    private javax.swing.JTextField jTextFieldFirstname;
    private javax.swing.JTextField jTextFieldLastname;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JTextField jTextFieldUsername;
    
    // access the database
    DBHandler db;
    
    // switch of new or existing user
    boolean isExistingUser;
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public UserFrame(String title, DBHandler db) {
        
        super("MyNotes - " + title);
        
        this.db = db;

        // Set variable based on whether a user is logged in or not
        // this allows creating two almost identical windows with same code
        if (db.isLoggedIn())
        {
            this.isExistingUser = true;
        }
        else
        {
            this.isExistingUser = false;
        }
        
        // initialize all the components in the frame
        initComponents();
        
        // don't allow risizing
        setResizable(false);
        
        // center the window at statup
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
    }


    @SuppressWarnings("unchecked")
    private void initComponents() {

        // create all ui objects
        jLabelFirstname = new javax.swing.JLabel();
        jLabelLastname = new javax.swing.JLabel();
        jLabelEmail = new javax.swing.JLabel();
        jLabelUsername = new javax.swing.JLabel();
        jLabelPassword1 = new javax.swing.JLabel();
        jLabelPassword2 = new javax.swing.JLabel();
        jTextFieldFirstname = new javax.swing.JTextField();
        jTextFieldLastname = new javax.swing.JTextField();
        jTextFieldEmail = new javax.swing.JTextField();
        jTextFieldUsername = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();
        jPasswordField2 = new javax.swing.JPasswordField();
        jButtonSave = new javax.swing.JButton();

        // set default action when x(close) is pressed
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        // set border colors
        Color colorBorder = new Color(90, 90, 90);  // dark gray
        Border border = BorderFactory.createLineBorder(colorBorder);
        jTextFieldFirstname.setBorder(border);
        jTextFieldLastname.setBorder(border);
        jTextFieldEmail.setBorder(border);
        jTextFieldUsername.setBorder(border);
        jPasswordField1.setBorder(border);
        jPasswordField2.setBorder(border);

        // set text on labels
        jLabelFirstname.setText("First name");
        jLabelLastname.setText("Last name");
        jLabelEmail.setText("Email");
        jLabelUsername.setText("Username");
        jLabelPassword1.setText("Password");
        jLabelPassword2.setText("Re-type");
        
        // if a user is logged in, then fill text fields with existing info
        if (this.isExistingUser)
        {   
            jTextFieldFirstname.setText(db.loggedInUser.firstname);
            jTextFieldLastname.setText(db.loggedInUser.lastname);
            jTextFieldEmail.setText(db.loggedInUser.email);
            jTextFieldUsername.setText(db.loggedInUser.username);
            jTextFieldUsername.setEditable(false);
        }
        if (this.isExistingUser)
            jButtonSave.setText("Save changes");
        else
            jButtonSave.setText("Create user");

        // "GUI-Klumpen"
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelFirstname)
                            .addComponent(jLabelLastname)
                            .addComponent(jLabelEmail)
                            .addComponent(jLabelUsername)
                            .addComponent(jLabelPassword1)
                            .addComponent(jLabelPassword2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldUsername, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldFirstname, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldLastname, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                            .addComponent(jTextFieldEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                            .addComponent(jPasswordField1)
                            .addComponent(jPasswordField2))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldFirstname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelFirstname))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldLastname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelLastname))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEmail))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelUsername))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPassword1)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPasswordField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPassword2))
                .addGap(9, 9, 9)
                .addComponent(jButtonSave)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        // end of "GUI-Klumpen"

        // create event handler for the button
        ButtonHandler buttonHandler = new ButtonHandler();
        jButtonSave.addActionListener(buttonHandler);
        
        pack();
        
        // set program logo
        ImageIcon image;
        image = new ImageIcon(getClass().getResource("mynotes_logo.png"));
        setIconImage(image.getImage());
    }

    private class ButtonHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (e.getSource() == jButtonSave)
            {
                String firstname = jTextFieldFirstname.getText();
                String lastname = jTextFieldLastname.getText();
                String email = jTextFieldEmail.getText();
                String username = jTextFieldUsername.getText();
                String password1 = jPasswordField1.getText();
                String password2 = jPasswordField2.getText();
                
                // remove whitespaces from passwords
                password1 = password1.replaceAll("\\s", "");
                password2 = password2.replaceAll("\\s", "");
                
                //----- Existing user -----
                if (isExistingUser)
                {
                    db.updateUser(db.loggedInUser, firstname, lastname, email);
                    boolean done = false;
                    // if something is entered in the password fields
                    if (password1.length() > 0 || password2.length() > 0)
                    {
                        if (passwordsValidated(password1, password2))
                        {   // passwords are validated
                            // change the password                            
                            db.changePassword(db.loggedInUser, password1);

                            done = true;
                        }
                    }
                    else
                    {
                        done = true;
                    }
                    
                    if (done)
                    {
                        // close the window
                        setVisible(false);
                            
                        // display dialog saying that the changes will take
                        // effect after a re-login
                        JOptionPane.showMessageDialog(null, String.format(""
                                        + "Your changes has been saved!\n\n"
                                        + "Please re-login to see changes take effect.", 
                                        null));
                    }
                }
                //----- New user -----
                else
                {
                    if (passwordsValidated(password1, password2) &&
                            usernameValidated(username))
                    {   // all criterias are met
                        // create the user
                        db.createUser(firstname, lastname, email, username, password1);
                        
                        // close the window
                        setVisible(false);
                    }
                }
            }
        }
    }
    
    
    //-------------------------------------------------
    // Function to validate two passwords
    // a dialog box is shown if something is wrong
    //-------------------------------------------------
    public boolean passwordsValidated(String password1, String password2)
    {
        // password is too short!
        if (password1.length() < 8)
        {
            JOptionPane.showMessageDialog(null, String.format(""
                    + "Your password is too short!\n\n"
                    + "It has to be of at least 8 characters in lenght.", 
                    null));
            return false;
        }
        
        // the passwords entered are not identical
        if (!password1.equals(password2))
        {
            JOptionPane.showMessageDialog(null, String.format(""
                    + "The passwords entered are not identical!\n\n"
                    + "Please try again.", 
                    null));
            return false;
        }
        
        return true;
    }

    
    //-------------------------------------------------
    // Function to validate a username
    // a dialog box is shown if something is wrong
    //-------------------------------------------------
    public boolean usernameValidated(String username)
    {
        // username is too short
        if (username.length() < 5)
        {
            JOptionPane.showMessageDialog(null, String.format(""
                    + "Your username is too short!\n\n"
                    + "It has to be of at least 5 characters in lenght.", 
                    null));
            return false;
        
        }
        
        // username already exists!
        if (db.userExists(username))
        {
            JOptionPane.showMessageDialog(null, String.format(""
                    + "Username '" + username + "' already exist!\n\n"
                    + "Please choose another username, and try again.", 
                    null));
            return false;
        }
        
        return true;
    }
}
