package dk.mercantec.om.mynotes;

public class User
{
    public String firstname, lastname, email, username;
    public int id;
    
    
    //-------------------------------------------------
    // Constructor
    //-------------------------------------------------
    public User(String firstname, String lastname, String email, String username, int id)
    {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.username = username;
        this.id = id;
    }
    
    
    //-------------------------------------------------
    // Function to return full name of the user
    //-------------------------------------------------
    public String getFullName()
    {
        return this.firstname + " " + this.lastname;
    }
}
