package dk.mercantec.om.mynotes;

import java.awt.Color;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class MyNotes
{
    //-------------------------------------------------
    // Main function
    //-------------------------------------------------
    public static void main(String[] args) throws SQLException, ClassNotFoundException
    {       
        // database login details
        String host = "";
        String database = "";
        String username = "";
        String password = "";

        // Set UI colors for all
        setUIColors();
        
        // connect to the database
        DBHandler db = new DBHandler(host, database, username, password);
        
        // if a connection could be made...
        if (db.isConnected())
        {
            // ...then display login form
            LoginFrame login = new LoginFrame(db);
            login.setVisible(true);
        }
        else
        {   // else, show a dialog box with info
            JOptionPane.showMessageDialog(null, String.format(
                            "A connection could not be made!\n\n"
                    + "This could either be a problem with your internet connection, \n"
                    + "or our server could be down at the moment. Please make sure \n"
                    + "that you are connected to the internet, or try again later.", 
                            null));
        }
        
    }
    
    
    //-------------------------------------------------
    // Function to set UI colors on all elements
    //-------------------------------------------------
    private static void setUIColors()
    {
        // define colors
        Color backColor = new Color(128,128,128);
        Color buttonColor = new Color(200,200,200);
        Color textColor = new Color(255,255,255);
        Color fieldColor = new Color(90,90,90);
        Color fieldHighlightColor = new Color(70,70,70);
        
        UIManager um = new UIManager();
        
        // colorize all the items
        um.put("OptionPane.background",backColor);
        um.put("OptionPane.messageForeground", textColor);
        um.put("Panel.background",backColor);
        um.put("ScrollPane.border", fieldColor);
        um.put("Button.background", buttonColor);
        um.put("Label.foreground", textColor );
        um.put("Panel.foreground", textColor);
        um.put("List.background",fieldColor);
        um.put("List.foreground",textColor);
        um.put("TextArea.background", fieldColor);
        um.put("TextArea.foreground", textColor);
        um.put("TextArea.border", fieldColor);
        um.put("TextField.background", fieldColor);
        um.put("TextField.foreground", textColor);
        um.put("TextField.border", textColor);
        um.put("PasswordField.background", fieldColor);
        um.put("PasswordField.foreground", textColor);
        um.put("PasswordField.border", textColor);
        um.put("CheckBox.background", backColor);
        um.put("ScrollBar.background", fieldColor);
        um.put("List.selectionBackground", fieldHighlightColor);
        um.put("List.selectionForeground", textColor);
        um.put("List.focusCellHighlightBorder", backColor);
    }
}
